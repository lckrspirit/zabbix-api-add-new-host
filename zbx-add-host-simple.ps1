# Zabbix server vars: 
$ZBX_SERVER = "<INSERT URL SERVER>"
$ZBX_API = "http://$ZBX_SERVER/api_jsonrpc.php"
$ZBX_TOKEN = "<INSERT API TOKEN>"
$ZBX_TEMPLATE = "Windows by Zabbix agent"
$ZBX_HOSTGRP = "Unassigned"

# Host vars:
$HOSTIP = "1.1.1.1"
$HOSTNAME = "TEst-api"
$AGENT_PORT = "10050"

$REQ_PARAMS = @{
    body =  @{
        "jsonrpc"= "2.0"
        "method"= "host.create"
        "params"= @{
            "host"= $HOSTNAME
            "interfaces"= @(
                @{
                "type"= 1
                "main"= 1
                "useip"= 1
                "ip"= $HOSTIP
                "dns"= ""
                "port"= $AGENT_PORT
                }
            )
            "groups"= @(
                @{
                "groupid"= "90"
                }
            )
        }
        "id"= 1
        "auth"= $ZBX_TOKEN
    } | ConvertTo-Json -Depth 5
    uri = "$ZBX_API"
    headers = @{"Content-Type" = "application/json"}
    method = "Post"
}

Invoke-WebRequest @REQ_PARAMS