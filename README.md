---
### zabbix-api-add-new-host

* `zbx-add-host-simple.ps1` - Скриптец, который просто добавляет хост в zabbix,
* `zbx-add-host.ps1` - Скриптец установит агента, и добавит новый хост в zabbix.

--- 

Как настроить, инстукция тут:
https://nixhub.ru/posts/zbx-auto-install/